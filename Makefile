all: hookfs file_hook.so

libdir ?= lib

hookfs: src/hook_fusefs/hookfs.c
	$(CC) -std=c99 -Wall `pkg-config fuse --cflags --libs` -lulockmgr \
		$(CFLAGS) $(LDFLAGS) src/hook_fusefs/hookfs.c -o hookfs

file_hook.so: file_hook.o
	ld -ldl $(RAW_LDFLAGS) -shared -o file_hook.so -lc file_hook.o

file_hook.o: src/hook_lib/file_hook.c
	$(CC) -Wall -fPIC -o file_hook.o -c src/hook_lib/file_hook.c

install:
	mkdir -p "${DESTDIR}/usr/$(libdir)/"
	cp file_hook.so "${DESTDIR}/usr/$(libdir)/"
	cp -R src/autodep "${DESTDIR}/usr/$(libdir)/"
	cp -R portage_with_autodep "${DESTDIR}/usr/$(libdir)/"

	mkdir -p "${DESTDIR}/usr/bin/"
	cp hookfs emerge_strict "${DESTDIR}/usr/bin/"
	ln -s "${DESTDIR}/usr/$(libdir)/autodep/autodep" "${DESTDIR}/usr/bin/"

clean:
	rm -f hookfs file_hook.o file_hook.so
