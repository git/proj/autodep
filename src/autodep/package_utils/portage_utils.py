#!/usr/bin/env python2

import os
import subprocess
import re

def getpackagesbyfiles(files):
  """
  :param files: list of filenames
  :returns: **dictionary** file->package, if file doesn't belong to any
    package it not returned as key of this dictionary
  """
  ret={}
  listtocheck=[]
  for f in files:
	if os.path.isdir(f):
	  ret[f]="directory"
	else:
	  listtocheck.append(f)
	  
  try:
	proc=subprocess.Popen(['qfile']+['--nocolor','--exact','','--from','-'],
	  stdin=subprocess.PIPE, stdout=subprocess.PIPE,stderr=subprocess.PIPE, 
	  bufsize=4096)
	
	out,err=proc.communicate(b"\n".join(listtocheck))
	if err!=None:
	  print "Noncritical error while launch qfile %s"%err;

	lines=out.split(b"\n")
	#print lines
	line_re=re.compile(r"^([^ ]+)\s+\(([^)]+)\)$")
	for line in lines:
	  try:
		line=line.decode("utf-8")
	  except UnicodeDecodeError:
		portage.util.writemsg("Util qfile returned non-utf8 string: %s\n" % line)

	  if len(line)==0:
		continue
	  match=line_re.match(line)
	  if match:
		try:
		  ret[match.group(2).encode("utf-8")]=match.group(1)
		except UnicodeEncodeError:
		  portage.util.writemsg(
			"Util qfile failed to encode string %s to unicode\n" % 
			match.group(2))
	  else:
		print "Util qfile returned unparsable string: %s" % line

  except OSError,e:
	print "Error while launching qfile: %s" % e
	
	
  return ret
  
def getfilesbypackage(packagename):
  """
  
  :param packagename: name of package
  :returns: **list** of files in package with name *packagename*
  """
  ret=[]
  try:
	proc=subprocess.Popen(['qlist']+['--nocolor',"--obj",packagename],
	  stdout=subprocess.PIPE,stderr=subprocess.PIPE, 
	  bufsize=4096)
	
	out,err=proc.communicate()
	if err!=None and len(err)!=0 :
	  print "Noncritical error while launch qlist: %s" % err;
	
	ret=out.split(b"\n")
	if ret==['']:
	  ret=[]
  except OSError,e:
	print "Error while launching qfile: %s" % e

  return ret
  
def get_all_packages_files():   
  """
  Memory-hungry operation
  
  :returns: **set** of all files that belongs to package
  """
  ret=[]
  try:
	proc=subprocess.Popen(['qlist']+['--all',"--obj"],
	  stdout=subprocess.PIPE,stderr=subprocess.PIPE, 
	  bufsize=4096)
	
	out,err=proc.communicate()
	if err!=None and len(err)!=0 :
	  print "Noncritical error while launch qlist: %s" % err;
	
	ret=out.split(b"\n")
  except OSError,e:
	print "Error while launching qfile: %s" % e

  return set(ret)
   
   