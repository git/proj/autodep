#!/usr/bin/env python

excluded_paths=set(
['/etc/sandbox.d/']
)

excluded_packages=set(
# autodep shows these two packages every time
['net-zope/zope-fixers', 'net-zope/zope-interface']
)

def is_file_excluded(f):
  for path in excluded_paths:
	if f.startswith(path): # if path is excluded
	  return True
  return False

def is_pkg_excluded(p):
  for pkg in excluded_packages:
	if p.startswith(pkg): # if package is excluded
	  return True
  return False


## some heuristics here to cut few packets
def is_package_useful(pkg,stages,files):
  if is_pkg_excluded(pkg):
	return False

  for f in files:
	if is_file_excluded(f):
	  continue
	
	# test 1: package is not useful if all files are *.desktop or *.xml or *.m4		
	if not (f.endswith(".desktop") or f.endswith(".xml") or f.endswith(".m4") or f.endswith(".pc")):
	  break
  else:
	return False # we get here if cycle ends not with break
	
  return True
  
