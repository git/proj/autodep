#!/usr/bin/env python

""" Output colorizing """

class color_printer:
  """
  A class for printing colored strings
  """
#  HEADER = '\033[95m'
#  OKBLUE = '\033[94m'
#  OKGREEN = '\033[92m'
#  FAIL = '\033[91m'
 
  ## \param enable_colors activate color output
  def __init__(self, enable_colors=True):
	if enable_colors:
	  self.COLOR2CODE={"warning":'\033[91m', "text":'\033[90m'}
	  self.ENDCOLOR='\033[0m'
	else:
	  self.COLOR2CODE={"warning":'', "text":''}
	  self.ENDCOLOR=''
  ## Prints a colored message
  # \param importance is a string "warning" or "text". Color depends on it
  # \param msg is a message to print
  def printmsg(self,importance,msg):
	print self.COLOR2CODE[importance] + str(msg) + self.ENDCOLOR,
