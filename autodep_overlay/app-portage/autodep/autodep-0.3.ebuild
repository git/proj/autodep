# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

PYTHON_DEPEND="2:2.6"
RESTRICT_PYTHON_ABIS='2.4 2.5 3.*'

inherit autotools eutils flag-o-matic multilib python

DESCRIPTION="Auto dependency analyser for Gentoo"
HOMEPAGE="http://soc.dev.gentoo.org/~bay/autodep/"
SRC_URI="http://soc.dev.gentoo.org/~bay/autodep/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="
	app-portage/portage-utils
	sys-fs/fuse"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

pkg_setup() {
	python_set_active_version 2
}

src_prepare() {
	epatch "${FILESDIR}"/${P}-build.patch
	sed "s:usr/lib:/usr/$(get_libdir):g" -i Makefile || die
	tc-export CC
	export raw_LDFLAGS="$(raw-ldflags)"
}

src_compile() {
    emake || die
}

src_install() {
    emake DESTDIR="${D}" install || die
}

pkg_postinst() {
	python_mod_optimize /usr/$(get_libdir)/portage_with_autodep
}

pkg_postrm() {
	python_mod_cleanup /usr/$(get_libdir)/portage_with_autodep
}

