# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

PYTHON_DEPEND="2:2.6"
RESTRICT_PYTHON_ABIS='2.4 2.5 3.*'

inherit autotools eutils python git-2 flag-o-matic multilib toolchain-funcs

DESCRIPTION="Auto dependency analyser for Gentoo"
HOMEPAGE="http://soc.dev.gentoo.org/~bay/autodep/"
SRC_URI=""
EGIT_REPO_URI="git://git.overlays.gentoo.org/proj/autodep.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
	app-portage/portage-utils
	sys-fs/fuse"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

pkg_setup() {
	python_set_active_version 2
}

src_compile() {
    emake CC="$(tc-getCC)" RAW_LDFLAGS="$(raw-ldflags)" || die
}

src_install() {
    emake DESTDIR="${D}" libdir="$(get_libdir)" install || die
}

pkg_postinst() {
	python_mod_optimize /usr/$(get_libdir)/portage_with_autodep
}

pkg_postrm() {
	python_mod_cleanup /usr/$(get_libdir)/portage_with_autodep
}

