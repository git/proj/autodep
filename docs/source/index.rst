.. Autodep documentation master file, created by
   sphinx-quickstart on Sat Jul 23 18:33:12 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Autodep's documentation
===================================

.. codeauthor:: Alexander Bersenev <bay@hackerdom.ru>



Contents
===================================

.. toctree::
   :maxdepth: 2

   Introduction <intro>
   Internals <architecture>
   API <api>

