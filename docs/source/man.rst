*************
USER COMMANDS
*************

SYNOPSIS
========

**autodep** [options] <command>

DESCRIPTION
===========

Auto dependency (autodep) builder is a tool for the analysis of accessed files during 
the build of a package. It also can be used for runtime dependencies analysis.

The tool can trace, log and block access to files of given packages.

OPTIONS
=======

.. program:: autodep

.. cmdoption:: --help, -h

   Show this help message and exit.

.. cmdoption:: -b, --block

   strict mode: Deny all access to files from non-dependency packages.

.. cmdoption:: --blockpkgs=PACKAGES

   Block access to files from this packages.

.. cmdoption:: -f, --files 

   Show all files, accessed and missing (not found).
.. cmdoption:: -v, --verbose 

   Show non-important packages, unknown packages and unknown building stages.
.. cmdoption:: --nocolor, -C

   Don't colorize output
.. cmdoption:: --hooklib

   Use LD_PRELOAD logging approach (default).
.. cmdoption:: --fusefs

   Use FUSE logging approach (slow, but reliable).

HOOKLIB VS FUSEFS
=================

+------------------------------------------------+-------------+---------------+
|                                                |   Hooklib   |     FuseFS    |
+================================================+=============+===============+
| Who can use this approach?                     | **Any user**|   Only root   |
+------------------------------------------------+-------------+---------------+
| Does approach allows to block access to files? |  **YES**    | **YES**       |
+------------------------------------------------+-------------+---------------+
| Is the overhead in performance noticeable?     |  **NO**     | YES [#f1]_    |
+------------------------------------------------+-------------+---------------+
| What events are logged?                        |  Most [#f2]_| **ALL**       |
+------------------------------------------------+-------------+---------------+
| When is it recomended to use an approach?      | Analysis    | Analysis      |
|                                                | of          | of *runtime*  |
|                                                | *buildtime* | dependencies  |
|                                                | dependencies|               |
+------------------------------------------------+-------------+---------------+
| Are there any pre-requirements                 | **NO**      | FUSE must be  |
| for using an approach?                         |             | enabled in    |
|                                                |             | kernel        |
+------------------------------------------------+-------------+---------------+

.. rubric:: Notes

.. [#f1] FUSE filesystems are slower than a normal one. A Program accesses many files 
   while launching, so this will take more time than usual.
.. [#f2] Loading of Dynamic libraries and direct syscalls will not be logged.

Examples
========

Get the potential dependencies for net-irc/xchat:
-------------------------------------------------
autodep emerge net-irc/xchat

Get the potential dependencies for net-irc/xchat, blocking files from x11-misc/util-macros package:
---------------------------------------------------------------------------------------------------
autodep emerge --blockpkgs x11-misc/util-macros emerge net-irc/xchat

Get the potential dependencies for net-irc/xchat package, and show accessed files:
----------------------------------------------------------------------------------
autodep --files emerge net-irc/xchat

Get the runtime dependencies of a xchat and show accessed files:
----------------------------------------------------------------
autodep --fusefs --files xchat

