************
Introduction
************

Overview
===================================
Auto dependency (autodep) builder is a tool for the analysis of accessed files during 
the build of a package. It also can be used for runtime dependencies analysis.

The tool can trace, log and block access to files of given packages.

It is released under GNU GPL license.

Status
===================================
Autodep is in active developing.

Installing
===================================
1. The package is available in "neurogeek" overlay. To add this overlay use:

.. code-block:: none
   
   layman -a neurogeek

2. Make sure that "source /var/lib/layman/make.conf" is in your /etc/make.conf
3. To install the package type:

.. code-block:: none
   
   emerge autodep

Running
===================================

.. program:: autodep

.. code-block:: none

    autodep [options] <command>

.. cmdoption:: --help, -h

   Show this help message and exit.

.. cmdoption:: -b, --block

   strict mode: Deny all access to files from non-dependency packages.

.. cmdoption:: --blockpkgs=PACKAGES

   Block access to files from this packages.

.. cmdoption:: -f, --files 

   Show all files, accessed and missing (not found).
.. cmdoption:: -v, --verbose 

   Show non-important packages, unknown packages and unknown building stages.
.. cmdoption:: --nocolor, -C

   Don't colorize output
.. cmdoption:: --hooklib

   Use LD_PRELOAD logging approach (default).
.. cmdoption:: --fusefs

   Use FUSE logging approach (slow, but reliable).

Example: showfsevents.py -b lsof,cowsay emerge bash

.. note:: 
   You can use **emerge_strict** command to check dependencies of a packages.
   The command has same syntax as emerge and launches bundled emerge with 
   *depcheckstrict* feature.

Hooklib vs Fusefs
===================================

+------------------------------------------------+-------------+---------------+
|                                                |   Hooklib   |     FuseFS    |
+================================================+=============+===============+
| Who can use this approach?                     | **Any user**|   Only root   |
+------------------------------------------------+-------------+---------------+
| Does approach allows to block access to files? |  **YES**    | **YES**       |
+------------------------------------------------+-------------+---------------+
| Is the overhead in performance noticeable?     |  **NO**     | YES [#f1]_    |
+------------------------------------------------+-------------+---------------+
| What events are logged?                        |  Most [#f2]_| **ALL**       |
+------------------------------------------------+-------------+---------------+
| When is it recomended to use an approach?      | Analysis    | Analysis      |
|                                                | of          | of *runtime*  |
|                                                | *buildtime* | dependencies  |
|                                                | dependencies|               |
+------------------------------------------------+-------------+---------------+
| Are there any pre-requirements                 | **NO**      | FUSE must be  |
| for using an approach?                         |             | enabled in    |
|                                                |             | kernel        |
+------------------------------------------------+-------------+---------------+

.. rubric:: Notes

.. [#f1] FUSE filesystems are slower than a normal one. A Program accesses many files 
   while launching, so this will take more time than usual.
.. [#f2] Loading of Dynamic libraries and direct syscalls will not be logged.

Examples
===================================
.. rubric:: 1. Get the potential dependencies for net-irc/xchat:

.. code-block:: none

    autodep emerge net-irc/xchat

.. rubric:: 2. Get the potential dependencies for net-irc/xchat, blocking 
   x11-misc/util-macros package:

.. code-block:: none

    autodep emerge --blockpkgs x11-misc/util-macros emerge net-irc/xchat

.. rubric:: 3. Get the potential dependencies for net-irc/xchat, and show files 
   accessed:

.. code-block:: none

    autodep --files emerge net-irc/xchat

.. rubric:: 4. Get the runtime dependencies of a xchat and show accessed files:

.. code-block:: none

    autodep --fusefs --files xchat



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

